#include "stdafx.h"
#include "StartMenu.h"

/* All this stuff was taken from the MSDN 'strtmenu' sample and adapted */

/**************************************************************************

   CreateLink()

   uses the shell's IShellLink and IPersistFile interfaces to create and 
   store a shortcut to the specified object. 
 
   Returns the result of calling the member functions of the interfaces. 
 
   lpszPathObj - address of a buffer containing the path of the object 

   lpszPathLink - address of a buffer containing the path where the shell 
      link is to be stored 

   lpszDesc - address of a buffer containing the description of the shell 
      link 
 
**************************************************************************/

HRESULT CreateLink( LPCSTR lpszSource, LPSTR lpszTarget, LPSTR lpszDesc )
{ 
	HRESULT hres;
	IShellLink* pShellLink;

	//CoInitialize must be called before this
	// Get a pointer to the IShellLink interface.
	hres = CoCreateInstance(   CLSID_ShellLink,
							   NULL,
							   CLSCTX_INPROC_SERVER,
							   IID_IShellLink,
							   (LPVOID*)&pShellLink);
	if ( SUCCEEDED( hres ) )
	{ 
		IPersistFile* pPersistFile;

		// Set the path to the shortcut target, and add the description.
		pShellLink->SetPath(lpszSource);
		pShellLink->SetDescription(lpszDesc);

		// Query IShellLink for the IPersistFile interface for saving the
		// shortcut in persistent storage.
		hres = pShellLink->QueryInterface(IID_IPersistFile, (LPVOID*)&pPersistFile);

		if (SUCCEEDED(hres))
		{
			WCHAR wsz[MAX_PATH]; 

			// Ensure that the string is ANSI. 
			MultiByteToWideChar( CP_ACP, 
							   0, 
							   lpszTarget, 
							   -1, 
							   wsz, 
							   MAX_PATH); 

			// Save the link by calling IPersistFile::Save. 
			hres = pPersistFile->Save(wsz, TRUE); 

//			if(FAILED(hres)) ErrorHandler();

			pPersistFile->Release(); 
		} 

		pShellLink->Release(); 
	}

	return hres;
}

/**************************************************************************

   DeleteLink()

**************************************************************************/

BOOL DeleteLink( HWND hWnd, LPSTR lpszShortcut )
{
	char  szFile[MAX_PATH];
	SHFILEOPSTRUCT fos;

	ZeroMemory(szFile, sizeof(szFile));
	lstrcpy(szFile, lpszShortcut);

	ZeroMemory(&fos, sizeof(fos));
	fos.hwnd = hWnd;
	fos.wFunc = FO_DELETE;
	fos.pFrom = szFile;
	fos.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT;
	SHFileOperation(&fos);

	return TRUE;
}
