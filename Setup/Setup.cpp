// Setup.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "StartMenu.h"

const char szInstall[] = "Teeuhr Installation";
const char szUninstall[] = "Teeuhr Deinstallation";
const char szAskInstall[] = "M�chten Sie die Teeuhr installieren?";
const char szAskUninstall[] = "M�chten Sie die Teeuhr wirklich deinstallieren?";
const char szTeeuhrWindowCaption[] = "Teeuhr";
const char szInstallCompleted[] = "Die Teeuhr wurde erfolgreich installiert!";
const char szUninstallCompleted[] = "Die Teeuhr wurde erfolgreich deinstalliert!";

//const char szErrorOnCopy[] = "Fehler #%lu beim Kopieren der Datei %s";
const char szErrorOnCreateUninstall[] = "Fehler #%ld beim Anlegen der Deinstallationsinformationen";
const char szErrorOnSHGetFolderPath[] = "Fehler #%ld beim Ausf�hren von SHGetFolderPath()";
const char szErrorOnCreateLink[]      = "Fehler beim Ausf�hren von CreateLink()";

const char* szCaption;
char szMsg[256];

static LPSTR WindowsFile( LPCSTR szFileName, LPSTR szPathName )
{
	GetWindowsDirectory( szPathName, MAX_PATH );

	//PathAppend( szPathName, szFileName );
	char* s = strrchr( szPathName, '\0' );
	if ( s > szPathName && s[-1] != '\\' )
		lstrcpy( s, "\\" );
	lstrcat( szPathName, szFileName );

	return szPathName;
}

BOOL CopyFileToWindowsDirectory( LPCSTR szExistingFileName, LPCSTR szNewFileName, LPSTR szPathName )
{
	WindowsFile( szNewFileName, szPathName );

#if 0

	if ( !CopyFile( szExistingFileName, szPathName, FALSE ) )
	{
		wsprintf( szMsg, szErrorOnCopy, GetLastError(), szNewFileName );
		MessageBox( 0, szMsg, szCaption, MB_ICONSTOP | MB_OK );
		return FALSE;
	}

#else

	SHFILEOPSTRUCT fos;
	ZeroMemory( &fos, sizeof(fos) );

	char szFrom[MAX_PATH];
	strrchr( lstrcpy( szFrom, szExistingFileName ), '\0' )[1] = '\0';
	strrchr( szPathName, '\0' )[1] = '\0';

	fos.hwnd = 0;
	fos.wFunc = FO_COPY;
	fos.pFrom = szFrom;
	fos.pTo   = szPathName;
	fos.fFlags = FOF_NOCONFIRMATION;

	if ( SHFileOperation( &fos ) != 0 )
		return FALSE;

#endif

	return TRUE;
}

VOID DeleteFileInWindowsDirectory( LPCSTR szFileName, LPSTR szPathName )
{
	DeleteFile( WindowsFile( szFileName, szPathName ) );
}

BOOL RegisterUninstall( LPCSTR szKeyName, LPCSTR szDisplayName, LPCSTR szUninstallString )
{
	char szSubKey[256];
	lstrcpy( szSubKey, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" );
	lstrcat( szSubKey, szKeyName );

	HKEY hk; DWORD dwDisposition;
	LONG Error = RegCreateKeyEx( HKEY_LOCAL_MACHINE, szSubKey, 0, 0, REG_OPTION_NON_VOLATILE,
		KEY_WRITE, 0, &hk, &dwDisposition );

	if ( Error != ERROR_SUCCESS )
	{
		wsprintf( szMsg, szErrorOnCreateUninstall, Error );
		MessageBox( 0, szMsg, szCaption, MB_ICONSTOP | MB_OK );
		return FALSE;
	}

	RegSetValueEx( hk, "DisplayName", 0, REG_SZ, (CONST BYTE*)szDisplayName, strlen( szDisplayName ) + 1 );
	RegSetValueEx( hk, "UninstallString", 0, REG_SZ, (CONST BYTE*)szUninstallString, strlen( szUninstallString ) + 1 );

	RegCloseKey( hk );
	return TRUE;
}

VOID UnregisterUninstall( LPCSTR szKeyName )
{
	char szSubKey[256];
	lstrcpy( szSubKey, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" );
	lstrcat( szSubKey, szKeyName );

	RegDeleteKey( HKEY_LOCAL_MACHINE, szSubKey );
}

int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
	BOOL Uninstall = ( lpCmdLine != 0 && (*lpCmdLine == '\"' || *lpCmdLine == 'T') );
	szCaption = (Uninstall) ? szUninstall : szInstall;

	LPCSTR szText = (Uninstall) ? szAskUninstall : szAskInstall;
	if ( MessageBox( 0, szText, szCaption, MB_ICONQUESTION | MB_YESNO ) != IDYES )
	{
		return 0;
	}

	HCURSOR hcurPrev = SetCursor( LoadCursor( 0, IDC_WAIT ) );

	HWND hWnd = FindWindow( 0, szTeeuhrWindowCaption );
	if ( hWnd != 0 )
	{
		SendMessage( hWnd, WM_CLOSE, 0, 0 );
		Sleep( 500 );
	}

	char szTeeuhr[MAX_PATH], szAsuninst[MAX_PATH + 2];
	char szShortcut[MAX_PATH];

#if 0

	// Needs IE 4.0 or higher!
	if ( !SHGetSpecialFolderPath( 0, szShortcut, CSIDL_STARTUP, FALSE ) )
	{
		MessageBox( 0, "Fehler beim Ausf�hren von SHGetSpecialFolderPath()", szCaption, MB_ICONSTOP | MB_OK );
		return 1;
	}

#else

	// Needs shfolder.dll from W2k (redistributable)
	HRESULT Result = SHGetFolderPath( 0, CSIDL_STARTUP, 0, SHGFP_TYPE_CURRENT, szShortcut );
	if ( Result != S_OK )
	{
		wsprintf( szMsg, szErrorOnSHGetFolderPath, Result );
		MessageBox( 0, szMsg, szCaption, MB_ICONSTOP | MB_OK );
		return 1;
	}

	/*
		Andere Idee:
		[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders]
		"Startup"="E:\\WINNT\\Profiles\\Axel\\Start Menu\\Programs\\Startup"
	*/

#endif

	//PathAppend( szShortcut, "Teeuhr.lnk" );
	lstrcat( szShortcut, "\\Teeuhr.lnk" );

	if ( !Uninstall )
	{
		if ( CopyFileToWindowsDirectory( "Teeuhr.ex_", "Teeuhr.exe", szTeeuhr ) &&
			 CopyFileToWindowsDirectory( "Setup.exe", "asuninst.exe", szAsuninst + 1 ) )
		{
			szAsuninst[0] = '\"';
			lstrcat( szAsuninst, "\" \"Teeuhr.asu\"" );

			if ( RegisterUninstall( "Teeuhr", "Teeuhr", szAsuninst ) )
			{
				CoInitialize( 0 );
				HRESULT hres = CreateLink( szTeeuhr, szShortcut, "" );
				CoUninitialize();

				if ( SUCCEEDED( hres ) )
				{
					ShellExecute( 0, "open", szTeeuhr, "", "", SW_SHOWDEFAULT );
					MessageBox( 0, szInstallCompleted, szCaption, MB_ICONINFORMATION | MB_OK );

					SetCursor( hcurPrev );
					return 0;
				}
				else
					MessageBox( 0, szErrorOnCreateLink, szCaption, MB_ICONSTOP | MB_OK );
			}
		}
	}

	DeleteLink( 0, szShortcut );

	for ( int i = 0; ; i++ )
	{
		char szSubKey[MAX_PATH], *sz;
		DWORD cbSubKey = MAX_PATH;
		FILETIME ftLastWriteTime;

		if ( ::RegEnumKeyEx( HKEY_USERS, i, szSubKey, &cbSubKey, 0, 0, 0, &ftLastWriteTime ) != ERROR_SUCCESS )
			break;
		sz = strrchr( szSubKey, '\0' );

		lstrcpy( sz, "\\Software\\Axel Sommerfeldt\\Teeuhr\\Settings" );
		RegDeleteKey( HKEY_USERS, szSubKey );

		lstrcpy( sz, "\\Software\\Axel Sommerfeldt\\Teeuhr" );
		RegDeleteKey( HKEY_USERS, szSubKey );
	}

	UnregisterUninstall( "Teeuhr" );

	DeleteFileInWindowsDirectory( "asuninst.exe", szAsuninst );
	DeleteFileInWindowsDirectory( "Teeuhr.exe", szTeeuhr );

	SetCursor( hcurPrev );

	if ( Uninstall )
	{
		MessageBox( 0, szUninstallCompleted, szCaption, MB_ICONINFORMATION | MB_OK );
		return 0;
	}

	return 2;
}
