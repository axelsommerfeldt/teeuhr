// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
//#include "Teeuhr.h"

#include "ShellIcon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CShellIcon

UINT CShellIcon::st_uID;

CShellIcon::CShellIcon()
{
	Clear();
}

CShellIcon::~CShellIcon()
{
	if ( m_hWnd != 0 )
		Delete();
}

BOOL CShellIcon::Add( HWND hWnd, UINT uCallbackMessage, HICON hIcon /*= 0*/, LPCTSTR lpszTip /*= 0*/ )
{
	m_hWnd = hWnd;
	m_uID = ++st_uID;
	m_uCallbackMessage = uCallbackMessage;

	return NotifyShellIcon( NIM_ADD, hIcon, lpszTip );
}

BOOL CShellIcon::Modify( HICON hIcon, LPCTSTR lpszTip )
{
	return NotifyShellIcon( NIM_MODIFY, hIcon, lpszTip );
}

BOOL CShellIcon::Delete()
{
	BOOL fResult = NotifyShellIcon( NIM_DELETE );
	if ( fResult ) Clear();
	return fResult;
}

BOOL CShellIcon::Restore()
{
	HICON hIcon = m_hIcon;
	CString strTip = m_strTip;

	m_hIcon = 0;
	m_strTip = "";
	return NotifyShellIcon( NIM_ADD, hIcon, strTip );
}

HICON CShellIcon::LoadIcon( UINT nID )
{
	return (HICON)LoadImage( AfxGetResourceHandle(), MAKEINTRESOURCE( nID ), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR );
}

BOOL CShellIcon::TrackPopupMenu( CMenu *pMenu, UINT nFlags, int x, int y, CWnd *pWnd, LPCRECT lpRect /*= 0*/ )
{
	// See also KB article Q135788
	// "PRB: Menus for Notification Icons Don't Work Correctly"
	pWnd->SetForegroundWindow();

	// See also MSJ 2/97 p26
	// "Keystroke invocation"
	if ( x == -1 && y == -1 )
	{
		CRect rect;
		pWnd->GetClientRect( rect );
		pWnd->ClientToScreen( rect );

		CPoint point = rect.TopLeft();
		point.Offset( 5, 5 );
		x = point.x; y = point.y;
	}

	// Popupmenu
	BOOL fResult = pMenu->TrackPopupMenu( nFlags, x, y, pWnd, lpRect );

	// See also KB article Q135788
	// "PRB: Menus for Notification Icons Don't Work Correctly"
	pWnd->PostMessage( WM_NULL );

	return fResult;
}

void CShellIcon::Clear()
{
	m_hWnd = 0;
	m_uID  = 0;
	m_uCallbackMessage = 0;
	m_hIcon = 0;
	m_strTip = "";
}

//#define NOTIFYICONDATA_SZTIP_SIZE 64

BOOL CShellIcon::NotifyShellIcon( DWORD dwMessage, HICON hIcon /*= 0*/, LPCTSTR lpszTip /*= 0*/ )
{
	NOTIFYICONDATA tnid;
	RtlZeroMemory( &tnid, sizeof( tnid ) );

	tnid.cbSize = sizeof( tnid );  // NOTIFYICONDATA_V2_SIZE
	tnid.hWnd   = m_hWnd;
	tnid.uID    = m_uID;

	if ( dwMessage == NIM_ADD )
	{
		tnid.uCallbackMessage = m_uCallbackMessage;
		tnid.uFlags |= NIF_MESSAGE;
	}

	if ( hIcon != 0 && hIcon != m_hIcon )
	{
		m_hIcon = hIcon;

		tnid.hIcon  = hIcon;
		tnid.uFlags |= NIF_ICON;
	}

	if ( lpszTip != 0 && m_strTip != lpszTip )
	{
		m_strTip = lpszTip;

		lstrcpyn( tnid.szTip, lpszTip, /*NOTIFYICONDATA_SZTIP_SIZE*/sizeof(tnid.szTip)/sizeof(tnid.szTip[0]) );
		TRACE( _T("tnid.szTip[%d] = \"%s\"\n"), sizeof(tnid.szTip)/sizeof(tnid.szTip[0]), tnid.szTip );
		tnid.uFlags |= NIF_TIP;
	}

	if ( dwMessage == NIM_MODIFY && tnid.uFlags == 0 )
		return TRUE;

#ifdef _DEBUG
	TRACE( _T("Shell_NotifyIcon( %s, %s )\n"), (LPCTSTR)GetMessageString( dwMessage ), (LPCTSTR)GetFlagsString( tnid.uFlags ) );
#endif

	return Shell_NotifyIcon( dwMessage, &tnid );
}

#ifdef _DEBUG

CString CShellIcon::GetMessageString( DWORD dwMessage )
{
	switch ( dwMessage )
	{
	case NIM_ADD:
		return "NIM_ADD";
	case NIM_MODIFY:
		return "NIM_MODIFY";
	case NIM_DELETE:
		return "NIM_DELETE";
	default:
		return "?";
	}
}

CString CShellIcon::GetFlagsString( UINT uFlags )
{
	CString strFlags;

	if ( (uFlags & NIF_MESSAGE) != 0 )
	{
		if ( !strFlags.IsEmpty() ) strFlags += '|';
		strFlags = "NIF_MESSAGE";
	}
	if ( (uFlags & NIF_ICON) != 0 )
	{
		if ( !strFlags.IsEmpty() ) strFlags += '|';
		strFlags = "NIF_ICON";
	}
	if ( (uFlags & NIF_TIP) != 0 )
	{
		if ( !strFlags.IsEmpty() ) strFlags += '|';
		strFlags = "NIF_TIP";
	}

	if ( strFlags.IsEmpty() )
		strFlags = "0";

	return strFlags;
}

#endif
