; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSettingsDlg
LastTemplate=CButton
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Teeuhr.h"
LastPage=0

ClassCount=5
Class1=CTeeuhrApp
Class3=CMainFrame
Class4=CAboutDlg

ResourceCount=7
Resource1=IDR_MAINFRAME (German (Germany))
Resource2=IDR_MAINFRAME
Class2=CChildView
Resource3=IDR_CONTEXT (German (Germany))
Resource4=IDD_SETTINGS (German (Germany))
Class5=CSettingsDlg
Resource5=IDD_ABOUTBOX (German (Germany))
Resource6=IDD_SETTINGS
Resource7=IDD_ABOUTBOX

[CLS:CTeeuhrApp]
Type=0
HeaderFile=Teeuhr.h
ImplementationFile=Teeuhr.cpp
Filter=N
LastObject=CTeeuhrApp

[CLS:CChildView]
Type=0
HeaderFile=ChildView.h
ImplementationFile=ChildView.cpp
Filter=N
LastObject=CChildView
BaseClass=CWnd 
VirtualFilter=WC

[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=CMainFrame
BaseClass=CFrameWnd
VirtualFilter=fWC




[CLS:CAboutDlg]
Type=0
HeaderFile=Teeuhr.cpp
ImplementationFile=Teeuhr.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CAboutDlg

[MNU:IDR_MAINFRAME (German (Germany))]
Type=1
Class=?
Command1=ID_APP_EXIT
Command2=ID_APP_ABOUT
CommandCount=2

[MNU:IDR_CONTEXT (German (Germany))]
Type=1
Class=CMainFrame
Command1=ID_CANCEL
Command2=ID_TIME5
Command3=ID_TIME4
Command4=ID_TIME3
Command5=ID_APP_SETTINGS
Command6=ID_APP_ABOUT
Command7=ID_APP_EXIT
CommandCount=7

[ACL:IDR_MAINFRAME (German (Germany))]
Type=1
Class=?
Command1=ID_EDIT_COPY
Command2=ID_EDIT_PASTE
Command3=ID_EDIT_UNDO
Command4=ID_EDIT_CUT
Command5=ID_NEXT_PANE
Command6=ID_PREV_PANE
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_CUT
Command10=ID_EDIT_UNDO
CommandCount=10

[DLG:IDD_ABOUTBOX (German (Germany))]
Type=1
Class=?
ControlCount=8
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_EMAIL,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_URL,static,1342308352
Control8=IDOK,button,1342373889

[DLG:IDD_SETTINGS (German (Germany))]
Type=1
Class=CSettingsDlg
ControlCount=11
Control1=IDC_OPTIC,button,1342373891
Control2=IDC_ACUSTIC,button,1342242819
Control3=IDC_BING,button,1342308361
Control4=IDC_X,edit,1350631552
Control5=IDC_SPIN,msctls_updown32,1342177314
Control6=IDC_SOUND,button,1342177289
Control7=IDC_FILE,edit,1350631424
Control8=IDC_SEARCH,button,1342242816
Control9=IDC_PLAY,button,1342242816
Control10=IDOK,button,1342373889
Control11=IDCANCEL,button,1342242816

[CLS:CSettingsDlg]
Type=0
HeaderFile=SettingsDlg.h
ImplementationFile=SettingsDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=IDOK
VirtualFilter=dWC

[DLG:IDD_ABOUTBOX]
Type=1
Class=?
ControlCount=6
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_EMAIL,static,1342308352
Control6=IDOK,button,1342373889

[DLG:IDD_SETTINGS]
Type=1
Class=CSettingsDlg
ControlCount=16
Control1=IDC_WINDOW,button,1342373891
Control2=IDC_STATIC_BGCOLOR,static,1342308352
Control3=IDC_BGCOLOR,button,1342242827
Control4=IDC_STATIC_FGCOLOR,static,1342308352
Control5=IDC_FGCOLOR,button,1342242827
Control6=IDC_SOUND,button,1342242819
Control7=IDC_BING_RADIO,button,1342308361
Control8=IDC_BING,edit,1350631552
Control9=IDC_BING_SPIN,msctls_updown32,1342177314
Control10=IDC_FILE_RADIO,button,1342177289
Control11=IDC_FILE,edit,1350633472
Control12=IDC_FILE_SEARCH,button,1342242816
Control13=IDC_FILE_PLAY,button,1342242816
Control14=IDOK,button,1342373889
Control15=IDCANCEL,button,1342242816
Control16=IDC_TEST,button,1342242816

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_CANCEL
Command2=ID_TIME10
Command3=ID_TIME8
Command4=ID_TIME6
Command5=ID_TIME5
Command6=ID_TIME4
Command7=ID_TIME3
Command8=ID_TIME2
Command9=ID_APP_SETTINGS
Command10=ID_APP_ABOUT
Command11=ID_APP_EXIT
CommandCount=11

