// SettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Teeuhr.h"
#include "MainFrm.h"
#include "SettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg dialog


CSettingsDlg::CSettingsDlg(CWnd *pParent /*=NULL*/)
	: CDialog(CSettingsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSettingsDlg)
	m_fWindow = FALSE;
	m_fSound = FALSE;
	m_nSound = -1;
	m_nBing = 0;
	m_strFileName = _T("");
	//}}AFX_DATA_INIT

	GetData();
}

void CSettingsDlg::DoDataExchange(CDataExchange *pDX)
{
	CDialog::DoDataExchange( pDX );

	//{{AFX_DATA_MAP(CSettingsDlg)
	DDX_Check(pDX, IDC_WINDOW, m_fWindow);
	DDX_Check(pDX, IDC_SOUND, m_fSound);
	DDX_Radio(pDX, IDC_BING_RADIO, m_nSound);
	DDX_Text(pDX, IDC_BING, m_nBing);
	DDV_MinMaxUInt(pDX, m_nBing, 1, 9);
	DDX_Text(pDX, IDC_FILE, m_strFileName);
	//}}AFX_DATA_MAP
}

void CSettingsDlg::EnableDlgItems()
{
	TRACE( "CSettingsDlg::EnableDlgItems()\n" );

	GetDlgItem( IDC_STATIC_BGCOLOR )->EnableWindow( m_fWindow );
	GetDlgItem( IDC_STATIC_FGCOLOR )->EnableWindow( m_fWindow );

	GetDlgItem( IDC_BING_RADIO )->EnableWindow( m_fSound );
	GetDlgItem( IDC_BING )->EnableWindow( m_fSound && m_nSound == 0 );
	GetDlgItem( IDC_BING_SPIN )->EnableWindow( m_fSound && m_nSound == 0 );
	GetDlgItem( IDC_FILE_RADIO )->EnableWindow( m_fSound );
	GetDlgItem( IDC_FILE )->EnableWindow( m_fSound && m_nSound == 1 );
	GetDlgItem( IDC_FILE_SEARCH )->EnableWindow( m_fSound && m_nSound == 1 );
	GetDlgItem( IDC_FILE_PLAY )->EnableWindow( m_fSound && m_nSound == 1 );

	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	ASSERT_KINDOF( CMainFrame, pFrame );
	GetDlgItem( IDC_TEST )->EnableWindow( pFrame->IsTestAvailable() );
}

BEGIN_MESSAGE_MAP(CSettingsDlg, CDialog)
	//{{AFX_MSG_MAP(CSettingsDlg)
	ON_BN_CLICKED(IDC_WINDOW, OnWindow)
	ON_BN_CLICKED(IDC_BGCOLOR, OnBgcolor)
	ON_BN_CLICKED(IDC_FGCOLOR, OnFgcolor)
	ON_BN_CLICKED(IDC_SOUND, OnSound)
	ON_BN_CLICKED(IDC_BING_RADIO, OnBing)
	ON_BN_CLICKED(IDC_FILE_RADIO, OnFile)
	ON_BN_CLICKED(IDC_FILE_SEARCH, OnFileSearch)
	ON_BN_CLICKED(IDC_FILE_PLAY, OnFilePlay)
	ON_BN_CLICKED(IDC_TEST, OnTest)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg message handlers

BOOL CSettingsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_cbBackground.Attach( IDC_BGCOLOR, this, m_crBackground );
	m_cbForeground.Attach( IDC_FGCOLOR, this, m_crForeground );

	EnableDlgItems();

	// associate button with edit item
	CSpinButtonCtrl *pSpin = (CSpinButtonCtrl *)GetDlgItem( IDC_BING_SPIN );
	ASSERT( pSpin != NULL );
	pSpin->SetBuddy( GetDlgItem( IDC_BING ) );
	pSpin->SetRange( 1, 9 );
	pSpin->SetPos( m_nBing );

	CEdit *pEdit = (CEdit *)GetDlgItem( IDC_FILE );
	pEdit->SetReadOnly();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSettingsDlg::OnWindow() 
{
	UpdateData( TRUE );
	EnableDlgItems();
}

void CSettingsDlg::OnBgcolor() 
{
	CColorDialog dlg( m_crBackground );
	if ( dlg.DoModal() == IDOK )
	{
		m_crBackground = dlg.GetColor();
		m_cbBackground.SetBGColor( m_crBackground, TRUE );
	}
}

void CSettingsDlg::OnFgcolor() 
{
	CColorDialog dlg( m_crForeground );
	if ( dlg.DoModal() == IDOK )
	{
		m_crForeground = dlg.GetColor();
		m_cbForeground.SetBGColor( m_crForeground, TRUE );
	}
}

void CSettingsDlg::OnSound() 
{
	UpdateData( TRUE );
	EnableDlgItems();
}

void CSettingsDlg::OnBing() 
{
	UpdateData( TRUE );
	EnableDlgItems();
}

void CSettingsDlg::OnFile() 
{
	UpdateData( TRUE );
	EnableDlgItems();
}

void CSettingsDlg::OnFileSearch() 
{
	CString strFilter;
	strFilter.LoadString( IDS_WAV_FILTER );

	CFileDialog dlg( TRUE, _T("wav"), m_strPathName, OFN_HIDEREADONLY, strFilter, this );

	if ( dlg.DoModal() == IDOK )
	{
		UpdateData( TRUE );

		m_strPathName = dlg.GetPathName();
		m_strFileName = dlg.GetFileName();

		UpdateData( FALSE );
	}
}

void CSettingsDlg::OnFilePlay() 
{
	CWaitCursor wc;

	GetDlgItem( IDC_FILE_PLAY )->EnableWindow( FALSE );
	PlaySound( m_strPathName, 0, SND_FILENAME|SND_NODEFAULT|SND_NOWAIT|SND_SYNC );
	GetDlgItem( IDC_FILE_PLAY )->EnableWindow( TRUE );
}

void CSettingsDlg::OnTest() 
{
	UpdateData( TRUE );
	SetData();

	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	ASSERT_KINDOF( CMainFrame, pFrame );
	pFrame->Test();
}

void CSettingsDlg::GetData()
{
	CTeeuhrApp *pApp = CTeeuhrApp::GetApp();

	m_fWindow      = pApp->m_fWindow;
	m_crBackground = pApp->m_crBackground;
	m_crForeground = pApp->m_crForeground;

	m_fSound       = pApp->m_fSound;
	m_nSound       = pApp->m_nSound;
	m_nBing        = pApp->m_nBing;
	m_strPathName  = pApp->m_strPathName;
	m_strFileName  = pApp->m_strFileName;
}

void CSettingsDlg::SetData()
{
	CTeeuhrApp *pApp = CTeeuhrApp::GetApp();

	pApp->m_fWindow      = m_fWindow;
	pApp->m_crBackground = m_crBackground;
	pApp->m_crForeground = m_crForeground;

	pApp->m_fSound       = m_fSound;
	pApp->m_nSound       = (CTeeuhrApp::Sound)m_nSound;
	pApp->m_nBing        = m_nBing;
	pApp->m_strPathName  = m_strPathName;
	pApp->m_strFileName  = m_strFileName;
}

void CSettingsDlg::OnOK() 
{
	UpdateData( TRUE );
	SetData();
	
	CDialog::OnOK();
}
