// ChildView.cpp : implementation of the CChildView class
//

#include "stdafx.h"
#include "Teeuhr.h"
#include "MainFrm.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildView

CChildView::CChildView()
{
}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView,CWnd )
	//{{AFX_MSG_MAP(CChildView)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), HBRUSH(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void CChildView::OnPaint() 
{
	CTeeuhrApp *pApp = CTeeuhrApp::GetApp();
	CMainFrame *pFrame = (CMainFrame *)GetParentFrame();
	CPaintDC dc(this); // device context for painting

	CRect rect; GetClientRect( &rect );
	CBrush brush;
	brush.CreateSolidBrush( pFrame->m_fInverse ? pApp->m_crForeground : pApp->m_crBackground );
	dc.FillRect( &rect, &brush );

	CSize size = dc.GetTextExtent( pApp->m_strAlert );
	dc.SetBkMode( TRANSPARENT );
	dc.SetTextColor( pFrame->m_fInverse ? pApp->m_crBackground : pApp->m_crForeground );
	dc.TextOut( (rect.right - size.cx) / 2, (rect.bottom - size.cy) / 2, pApp->m_strAlert );
}
