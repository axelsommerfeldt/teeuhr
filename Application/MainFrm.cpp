// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Teeuhr.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define WM_NOTIFYICON    (WM_USER + 51)
#define WM_SOUND_PLAYED  (WM_USER + 52)

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

const UINT s_uTaskbarCreated = RegisterWindowMessage( TEXT("TaskbarCreated") );

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	ON_UPDATE_COMMAND_UI(ID_CANCEL, OnUpdateCancel)
	ON_UPDATE_COMMAND_UI(ID_TIME2, OnUpdateTime2)
	ON_COMMAND(ID_TIME2, OnTime2)
	ON_UPDATE_COMMAND_UI(ID_TIME3, OnUpdateTime3)
	ON_COMMAND(ID_TIME3, OnTime3)
	ON_UPDATE_COMMAND_UI(ID_TIME4, OnUpdateTime4)
	ON_COMMAND(ID_TIME4, OnTime4)
	ON_UPDATE_COMMAND_UI(ID_TIME5, OnUpdateTime5)
	ON_COMMAND(ID_TIME5, OnTime5)
	ON_UPDATE_COMMAND_UI(ID_TIME6, OnUpdateTime6)
	ON_COMMAND(ID_TIME6, OnTime6)
	ON_UPDATE_COMMAND_UI(ID_TIME8, OnUpdateTime8)
	ON_COMMAND(ID_TIME8, OnTime8)
	ON_UPDATE_COMMAND_UI(ID_TIME10, OnUpdateTime10)
	ON_COMMAND(ID_TIME10, OnTime10)
	ON_COMMAND(ID_CANCEL, OnCancel)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_NOTIFYICON, OnNotifyIcon)
	ON_REGISTERED_MESSAGE(s_uTaskbarCreated, OnTaskbarCreated)
	ON_MESSAGE(WM_SOUND_PLAYED, OnSoundPlayed)
END_MESSAGE_MAP()

#define TIMER_ID     1

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_fTimerIsRunning = FALSE;
	m_pthreadSound = 0;
}

CMainFrame::~CMainFrame()
{
	;
}

BOOL CMainFrame::DestroyWindow() 
{
	while ( m_pthreadSound != 0 )
		Sleep( 0 );

	StopTimer();
	m_icon.Delete();
	
	return CFrameWnd::DestroyWindow();
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.hMenu = 0; cs.hwndParent = 0;
	cs.style = WS_OVERLAPPED | WS_CAPTION | WS_THICKFRAME | WS_SYSMENU;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
//	cs.lpszClass = AfxRegisterWndClass(0);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void *pExtra, AFX_CMDHANDLERINFO *pHandlerInfo)
{
	// let the view have first crack at the command
	if ( m_wndView.OnCmdMsg( nID, nCode, pExtra, pHandlerInfo ) )
		return TRUE;

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg( nID, nCode, pExtra, pHandlerInfo );
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame operations

void CMainFrame::UpdateShellIcon( int seconds_left /* =0 */)
{
	CString strTip;
	HICON hIcon;

	if ( m_fTimerIsRunning )
	{
		if ( seconds_left >= 0 )
		{
			strTip = GetToolTip( IDS_RUNNING_MIN_OR_SEC, IDS_RUNNING_MIN_AND_SEC, seconds_left );
			hIcon = m_hIcon[(seconds_left + 59) / 60];
		}
		else
		{
			strTip = GetToolTip( IDS_OVERDUE_MIN_OR_SEC, IDS_OVERDUE_MIN_AND_SEC, -seconds_left );
			hIcon = m_hIcon[0];
		}
	}
	else
	{
		strTip.LoadString( IDS_INACTIVE );
		hIcon = m_hMainIcon;
	}

	m_icon.Modify( hIcon, strTip );
}

CString CMainFrame::GetToolTip( UINT nMinOrSecID, UINT nMinAndSecID, int seconds_left )
{
	int minutes = seconds_left / 60;
	int seconds = seconds_left % 60;

	CString strMinutesLeft;
	CString strSecondsLeft;
	CString strMinutesTotal;

	strMinutesLeft.LoadString( (minutes == 1) ? IDS_MINUTE : IDS_MINUTES );
	strSecondsLeft.LoadString( (seconds == 1) ? IDS_SECOND : IDS_SECONDS );
	strMinutesTotal.LoadString( (m_nMinutes == 1) ? IDS_MINUTE : IDS_MINUTES );

	CString strTip;
	if ( minutes == 0 )
		strTip.Format( nMinOrSecID, seconds, (LPCTSTR)strSecondsLeft, m_nMinutes, (LPCTSTR)strMinutesTotal );
	else if ( seconds == 0 )
		strTip.Format( nMinOrSecID, minutes, (LPCTSTR)strMinutesLeft, m_nMinutes, (LPCTSTR)strMinutesTotal );
	else
		strTip.Format( nMinAndSecID, minutes, (LPCTSTR)strMinutesLeft, seconds, (LPCTSTR)strSecondsLeft, m_nMinutes, (LPCTSTR)strMinutesTotal );

	TRACE( _T("GetToolTip() = \"%s\"\n"), (LPCTSTR)strTip );
	return strTip;
}

void CMainFrame::StartTimer( int nMinutes )
{
	UINT uElapse = GetCaretBlinkTime();
	TRACE( "GetCaretBlinkTime() = %u / %#x\n", uElapse, uElapse );
	if ( uElapse == 0 || uElapse > 1000 ) uElapse = 1000;

	m_nMinutes = nMinutes;
	m_fInverse = -1;
	m_dwTickCount = GetTickCount();

	InterlockedExchange( &m_fTimerIsRunning, TRUE );
	if ( SetTimer( TIMER_ID, uElapse, 0 ) )
	{
		UpdateShellIcon( m_nMinutes * 60 );
	}
	else
	{
		InterlockedExchange( &m_fTimerIsRunning, FALSE );
		MessageBeep( (UINT)-1 );
	}
}

void CMainFrame::StopTimer()
{
	if ( InterlockedExchange( &m_fTimerIsRunning, FALSE ) )
	{
		KillTimer( TIMER_ID );
		UpdateShellIcon();
	}
}

bool CMainFrame::IsTestAvailable()
{
	return !m_fTimerIsRunning;
}

void CMainFrame::Test()
{
	if ( IsTestAvailable() )
		OnTime( 0 );
	else
		MessageBeep( (UINT)-1 );
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame implementation

UINT AFX_CDECL CMainFrame::SoundProc( LPVOID pParam )
{
	CMainFrame *pFrame = (CMainFrame *)pParam;
	CTeeuhrApp *pApp = CTeeuhrApp::GetApp();

	UINT i;

	switch ( pApp->m_nSound )
	{
	case CTeeuhrApp::MESSAGE_BEEP:
		for ( i = 0; i < pApp->m_nBing; i++ )
			MessageBeep( (UINT)-1 );
		break;

	case CTeeuhrApp::PLAY_SOUND_FILE:
		PlaySound( pApp->m_strPathName, 0, SND_FILENAME|SND_SYNC );
		break;
	}

	Sleep( 1000 );
	pFrame->PostMessage( WM_SOUND_PLAYED );

	pFrame->m_pthreadSound = 0;
	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message map functions

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if ( CFrameWnd::OnCreate(lpCreateStruct) == -1 )
		return -1;

	CTeeuhrApp *pApp = CTeeuhrApp::GetApp();
	CRect rect;

	SystemParametersInfo( SPI_GETWORKAREA, 0, (RECT*)&rect, 0 );
	TRACE( "SPI_GETWORKAREA: %d, %d, %d, %d\n", rect.left, rect.top, rect.right, rect.bottom );

	SetWindowPos( 0, rect.right - pApp->m_cx, rect.bottom - pApp->m_cy, pApp->m_cx, pApp->m_cy,
		SWP_HIDEWINDOW | SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER );

	// create a view to occupy the client area of the frame
	if ( !m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL ) )
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}

	m_hMainIcon = CShellIcon::LoadIcon( IDR_MAINFRAME );
	for ( int i = 0; i <= 10; i++ )
		m_hIcon[i]  = CShellIcon::LoadIcon( IDI_ICON00 + i );

	m_icon.Add( GetSafeHwnd(), WM_NOTIFYICON, m_hMainIcon, AfxGetAppName() );
	return 0;
}

void CMainFrame::OnSetFocus(CWnd * /*pOldWnd*/)
{
	// forward focus to the view window
	m_wndView.SetFocus();
}

void CMainFrame::OnSysCommand( UINT nID, LONG lParam )
{
	if ( nID == SC_CLOSE )
	{
		OnCancel();
		return;
	}

	CFrameWnd::OnSysCommand( nID, lParam );
}

void CMainFrame::OnUpdateCancel(CCmdUI *pCmdUI) 
{
	pCmdUI->Enable( m_fTimerIsRunning );
}

void CMainFrame::OnCancel() 
{
	StopTimer();
	ShowWindow( SW_HIDE );
}

void CMainFrame::OnUpdateTime2(CCmdUI *pCmdUI) 
{
	OnUpdateTime( pCmdUI );
}

void CMainFrame::OnTime2() 
{
	OnTime( 2 );
}

void CMainFrame::OnUpdateTime3(CCmdUI *pCmdUI) 
{
	OnUpdateTime( pCmdUI );
}

void CMainFrame::OnTime3() 
{
	OnTime( 3 );
}

void CMainFrame::OnUpdateTime4(CCmdUI *pCmdUI) 
{
	OnUpdateTime( pCmdUI );
}

void CMainFrame::OnTime4() 
{
	OnTime( 4 );
}

void CMainFrame::OnUpdateTime5(CCmdUI *pCmdUI) 
{
	OnUpdateTime( pCmdUI );
}

void CMainFrame::OnTime5() 
{
	OnTime( 5 );
}

void CMainFrame::OnUpdateTime6(CCmdUI *pCmdUI) 
{
	OnUpdateTime( pCmdUI );
}

void CMainFrame::OnTime6() 
{
	OnTime( 6 );
}

void CMainFrame::OnUpdateTime8(CCmdUI *pCmdUI) 
{
	OnUpdateTime( pCmdUI );
}

void CMainFrame::OnTime8() 
{
	OnTime( 8 );
}

void CMainFrame::OnUpdateTime10(CCmdUI *pCmdUI) 
{
	OnUpdateTime( pCmdUI );
}

void CMainFrame::OnTime10() 
{
	OnTime( 10 );
}

void CMainFrame::OnUpdateTime(CCmdUI *pCmdUI)
{
	pCmdUI->Enable( !m_fTimerIsRunning );
}

void CMainFrame::OnTime( int nMinutes )
{
	OnCancel();
	StartTimer( nMinutes );
}

void CMainFrame::OnTimer( UINT nIDEvent )
{
	TRACE( "CMainFrame::OnTimer()\n" );

	if ( nIDEvent == TIMER_ID )
	{
		CTeeuhrApp *pApp = CTeeuhrApp::GetApp();

		int seconds_left = m_nMinutes * 60 - (int)((GetTickCount() - m_dwTickCount) / 1000);
		UpdateShellIcon( seconds_left );

		if ( seconds_left <= 0 )
		{
			if ( m_fInverse < 0 )
			{
				m_fInverse = FALSE;

				if ( pApp->m_fWindow )
				{
					ActivateFrame();
					SetForegroundWindow();
					BringWindowToTop();
				}

				if ( pApp->m_fSound )
				{
					m_pthreadSound = AfxBeginThread( SoundProc, this );
				}
			}
			else if ( pApp->m_fWindow )
			{
				FlashWindow( TRUE );
				m_fInverse = !m_fInverse;
				m_wndView.Invalidate( FALSE );
			}
		}
	}
	else
	{
		CFrameWnd::OnTimer( nIDEvent );
	}
}

LRESULT CMainFrame::OnNotifyIcon( WPARAM wParam, LPARAM lParam )
{
	if ( wParam != m_icon.GetID() ) return 0;
//	ASSERT( wParam == m_icon.GetID() );
//	UNUSED( wParam );

	CPoint point;
	CMenu menu;

	switch ( lParam )
	{
	case WM_LBUTTONDBLCLK:
		CTeeuhrApp::GetApp()->OnAppSettings();
		break;

	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		GetCursorPos( &point );
		if ( menu.LoadMenu( IDR_MAINFRAME ) )
		{
			CMenu *pPopup = menu.GetSubMenu( 0 );
			ASSERT( pPopup != NULL );

			MENUITEMINFO mmi;
			mmi.cbSize = sizeof mmi;
			mmi.fMask  = MIIM_STATE;
			if ( pPopup->GetMenuItemInfo( ID_APP_SETTINGS, &mmi ) )
			{
				mmi.cbSize = sizeof mmi;
				mmi.fMask  = MIIM_STATE;
				mmi.fState |= MFS_DEFAULT;
				::SetMenuItemInfo( pPopup->GetSafeHmenu(), ID_APP_SETTINGS, FALSE, &mmi );
			}

			// Popupmenu
			m_icon.TrackPopupMenu( pPopup, TPM_RIGHTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this );
		}
		else
			MessageBeep( (UINT)-1 );

		break;
	}

	return 0;
}

LRESULT CMainFrame::OnTaskbarCreated( WPARAM, LPARAM )
{
	m_icon.Restore();
	UpdateShellIcon();

	return 0;
}

LRESULT CMainFrame::OnSoundPlayed( WPARAM, LPARAM )
{
	CTeeuhrApp *pApp = CTeeuhrApp::GetApp();
	if ( !pApp->m_fWindow ) StopTimer();

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
