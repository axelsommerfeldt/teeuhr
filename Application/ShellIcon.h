class CShellIcon
{
public:
	CShellIcon();
	~CShellIcon();

	UINT GetID() const { return m_uID; }

	BOOL Add( HWND hWnd, UINT uCallbackMessage, HICON hIcon = 0, LPCTSTR lpszTip = 0 );
	BOOL Modify( HICON hIcon ) { return Modify( hIcon, 0 ); }
	BOOL Modify( LPCTSTR lpszTip ) { return Modify( 0, lpszTip ); }
	BOOL Modify( HICON hIcon, LPCTSTR lpszTip );
	BOOL Delete();
	BOOL Restore();

	static HICON LoadIcon( UINT nID );
	static BOOL TrackPopupMenu( CMenu *pMenu, UINT nFlags, int x, int y, CWnd *pWnd, LPCRECT lpRect = 0 );

protected:
	void Clear();
	BOOL NotifyShellIcon( DWORD dwMessage, HICON hIcon = 0, LPCTSTR pszTip = 0 );
#ifdef _DEBUG
	CString GetMessageString( DWORD dwMessage );
	CString GetFlagsString( UINT uFlags );
#endif

protected:
	HWND  m_hWnd;
	UINT  m_uID;
	UINT  m_uCallbackMessage;
	HICON m_hIcon;
	CString m_strTip;

	static UINT st_uID;
};
