// Teeuhr.h : main header file for the TEEUHR application
//

#if !defined(AFX_TEEUHR_H__741901D7_74FD_11D4_80D5_000000000000__INCLUDED_)
#define AFX_TEEUHR_H__741901D7_74FD_11D4_80D5_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CTeeuhrApp:
// See Teeuhr.cpp for the implementation of this class
//

class CTeeuhrApp : public CWinApp
{
public:
	CTeeuhrApp();

	static CTeeuhrApp *GetApp() { return (CTeeuhrApp *)AfxGetApp(); }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTeeuhrApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	CDialog *m_pActiveDlg;
	int DoModal( CDialog *pDialog );

// Settings
	BOOL m_fWindow;
	CString m_strAlert;
	int  m_cx, m_cy;

	BOOL m_fSound;
	enum Sound { NO_SOUND = -1, MESSAGE_BEEP = 0, PLAY_SOUND_FILE = 1 } m_nSound;
	UINT m_nBing;
	CString m_strPathName, m_strFileName;

	COLORREF m_crBackground, m_crForeground;

	void LoadProfileSettings();
	void SaveProfileSettings();

public:
	//{{AFX_MSG(CTeeuhrApp)
	afx_msg void OnAppAbout();
	afx_msg void OnAppSettings();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEEUHR_H__741901D7_74FD_11D4_80D5_000000000000__INCLUDED_)
