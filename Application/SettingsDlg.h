#if !defined(AFX_SETTINGSDLG_H__F97F04E1_750D_11D4_80D5_000000000000__INCLUDED_)
#define AFX_SETTINGSDLG_H__F97F04E1_750D_11D4_80D5_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SettingsDlg.h : header file
//

#include "colorBtn.h"

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg dialog

class CSettingsDlg : public CDialog
{
// Construction
public:
	CSettingsDlg(CWnd *pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSettingsDlg)
	enum { IDD = IDD_SETTINGS };
	BOOL	m_fWindow;
	BOOL	m_fSound;
	int		m_nSound;
	UINT	m_nBing;
	CString	m_strFileName;
	//}}AFX_DATA

	CString m_strPathName;
	COLORREF m_crBackground, m_crForeground;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSettingsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange *pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CColorButton m_cbBackground, m_cbForeground;

	void EnableDlgItems();
	void GetData();
	void SetData();

	// Generated message map functions
	//{{AFX_MSG(CSettingsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnWindow();
	afx_msg void OnBgcolor();
	afx_msg void OnFgcolor();
	afx_msg void OnSound();
	afx_msg void OnBing();
	afx_msg void OnFile();
	afx_msg void OnFileSearch();
	afx_msg void OnFilePlay();
	afx_msg void OnTest();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETTINGSDLG_H__F97F04E1_750D_11D4_80D5_000000000000__INCLUDED_)
