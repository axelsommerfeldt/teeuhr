// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__741901DB_74FD_11D4_80D5_000000000000__INCLUDED_)
#define AFX_MAINFRM_H__741901DB_74FD_11D4_80D5_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ChildView.h"
#include "ShellIcon.h"

class CMainFrame : public CFrameWnd
{
	
public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

// Attributes
protected:
	HICON m_hMainIcon, m_hIcon[11];
	CShellIcon m_icon;

	LONG  m_fTimerIsRunning;
	DWORD m_dwTickCount;
	int   m_nMinutes;
	CWinThread *m_pthreadSound;

// Operations
protected:
	void  UpdateShellIcon( int seconds_left = 0 );
	CString GetToolTip( UINT nMinOrSecID, UINT nMinAndSecID, int seconds_left );

	void  StartTimer( int nMinutes );
	void  StopTimer();

public:
	bool  IsTestAvailable();
	void  Test();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
	static UINT AFX_CDECL SoundProc( LPVOID pParam );

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	CChildView m_wndView;
	int m_fInverse;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnSysCommand(UINT nID,LONG lParam);
	afx_msg void OnUpdateCancel(CCmdUI *pCmdUI);
	afx_msg void OnCancel();
	afx_msg void OnUpdateTime2(CCmdUI *pCmdUI);
	afx_msg void OnTime2();
	afx_msg void OnUpdateTime3(CCmdUI *pCmdUI);
	afx_msg void OnTime3();
	afx_msg void OnUpdateTime4(CCmdUI *pCmdUI);
	afx_msg void OnTime4();
	afx_msg void OnUpdateTime5(CCmdUI *pCmdUI);
	afx_msg void OnTime5();
	afx_msg void OnUpdateTime6(CCmdUI *pCmdUI);
	afx_msg void OnTime6();
	afx_msg void OnUpdateTime8(CCmdUI *pCmdUI);
	afx_msg void OnTime8();
	afx_msg void OnUpdateTime10(CCmdUI *pCmdUI);
	afx_msg void OnTime10();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	afx_msg LRESULT OnNotifyIcon( WPARAM, LPARAM );
	afx_msg LRESULT OnTaskbarCreated( WPARAM, LPARAM );
	afx_msg LRESULT OnSoundPlayed( WPARAM, LPARAM );
	DECLARE_MESSAGE_MAP()

	void OnUpdateTime(CCmdUI *pCmdUI);
	void OnTime( int nMinutes );
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__741901DB_74FD_11D4_80D5_000000000000__INCLUDED_)
