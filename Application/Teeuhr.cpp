// Teeuhr.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Teeuhr.h"

#include "MainFrm.h"
#include "SettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define REGISTRY_KEY  "Axel Sommerfeldt"
#define E_MAIL "axel.sommerfeldt@f-m.fm"

/////////////////////////////////////////////////////////////////////////////
// CTeeuhrApp

BEGIN_MESSAGE_MAP(CTeeuhrApp, CWinApp)
	//{{AFX_MSG_MAP(CTeeuhrApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_APP_SETTINGS, OnAppSettings)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTeeuhrApp construction

CTeeuhrApp::CTeeuhrApp()
{
	m_pActiveDlg = 0;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTeeuhrApp object

CTeeuhrApp theApp;
static const TCHAR g_szSettings[] = _T("Settings");

/////////////////////////////////////////////////////////////////////////////
// CTeeuhrApp initialization

BOOL CTeeuhrApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#if 0  // warning C4996: 'CWinApp::Enable3dControls(Static)': CWinApp::Enable3dControls(Static) is no longer needed. You should remove this call.
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T(REGISTRY_KEY));
	LoadProfileSettings();

	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object.

//#define TEST
#ifdef TEST
	OnAppSettings();
	return FALSE;
#else

	CMainFrame *pFrame = new CMainFrame;
	m_pMainWnd = pFrame;

	// create and load the frame with its resources

	pFrame->LoadFrame( IDR_MAINFRAME, WS_OVERLAPPEDWINDOW, 0, 0 );

	// The one and only window has been initialized, so show and update it.
	pFrame->ShowWindow( SW_HIDE /*SW_SHOW*/ );
	pFrame->UpdateWindow();

	return TRUE;
#endif
}

void CTeeuhrApp::LoadProfileSettings()
{
	CString strAlert; strAlert.LoadString( IDS_TEEISREADY );

	m_fWindow     = GetProfileInt   ( g_szSettings, _T("fWindow"),     TRUE );
	m_strAlert    = GetProfileString( g_szSettings, _T("strAlert"),    strAlert );
	m_cx          = GetProfileInt   ( g_szSettings, _T("cx"),          150 );
	m_cy          = GetProfileInt   ( g_szSettings, _T("cy"),          100 );

	m_fSound      = GetProfileInt   ( g_szSettings, _T("fSound"),      TRUE );
	m_nSound  = (Sound)GetProfileInt( g_szSettings, _T("nSound"),      MESSAGE_BEEP /* Bing!*/ );
	m_nBing       = GetProfileInt   ( g_szSettings, _T("nBing"),       3 );
	m_strPathName = GetProfileString( g_szSettings, _T("strPathName"), _T("") );
	m_strFileName = GetProfileString( g_szSettings, _T("strFileName"), _T("") );

	CString strColor;
	int r,g,b;

	strColor      = GetProfileString( g_szSettings, _T("crBackground"), _T("255,255,255") );
	_stscanf( strColor, _T("%d,%d,%d"), &r, &g, &b );
	m_crBackground = RGB( r, g, b );
	strColor      = GetProfileString( g_szSettings, _T("crForeground"), _T("0,0,0") );
	_stscanf( strColor, _T("%d,%d,%d"), &r, &g, &b );
	m_crForeground = RGB( r, g, b );
}

void CTeeuhrApp::SaveProfileSettings()
{
	WriteProfileInt   ( g_szSettings, _T("fWindow"),     m_fWindow );
	WriteProfileString( g_szSettings, _T("strAlert"),    m_strAlert );
	WriteProfileInt   ( g_szSettings, _T("cx"),          m_cx );
	WriteProfileInt   ( g_szSettings, _T("cy"),          m_cy );

	WriteProfileInt   ( g_szSettings, _T("fSound"),      m_fSound );
	WriteProfileInt   ( g_szSettings, _T("nSound"),      m_nSound );
	WriteProfileInt   ( g_szSettings, _T("nBing"),       m_nBing );
	WriteProfileString( g_szSettings, _T("strPathName"), m_strPathName );
	WriteProfileString( g_szSettings, _T("strFileName"), m_strFileName );

	CString strColor;
	COLORREF rgb;

	rgb = m_crBackground;
	strColor.Format( _T("%d,%d,%d"), GetRValue(rgb), GetGValue(rgb), GetBValue(rgb) );
	WriteProfileString( g_szSettings, _T("crBackground"), strColor );
	rgb = m_crForeground;
	strColor.Format( _T("%d,%d,%d"), GetRValue(rgb), GetGValue(rgb), GetBValue(rgb) );
	WriteProfileString( g_szSettings, _T("crForeground"), strColor );
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

#include "StatLink.h"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg(CWnd *pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CStaticLink m_linkEmail, m_linkWeb;

	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg(CWnd *pParent /*=NULL*/)
	: CDialog(CAboutDlg::IDD, pParent)
	, m_linkEmail( _T("mailto:")_T(E_MAIL) )
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange *pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTeeuhrApp message handlers

// App command to run the dialog
void CTeeuhrApp::OnAppAbout()
{
	// Note:
	// We use GetDesktopWindow() here so the user is able to close
	// the main window even if the settings dialog is open.
	CAboutDlg dlg( CWnd::FromHandle( GetDesktopWindow() ) );

	DoModal( &dlg );
}

void CTeeuhrApp::OnAppSettings()
{
	// Note:
	// We use GetDesktopWindow() here so the user is able to close
	// the main window even if the settings dialog is open.
	CSettingsDlg dlg( CWnd::FromHandle( GetDesktopWindow() ) );

	if ( DoModal( &dlg ) == IDOK )
		SaveProfileSettings();
	else
		LoadProfileSettings();
}

int CTeeuhrApp::DoModal( CDialog *pDialog )
{
	int error;

	if ( m_pActiveDlg != 0 )
	{
		m_pActiveDlg->SetForegroundWindow();
		m_pActiveDlg->BringWindowToTop();
		MessageBeep( (UINT)-1 );

		error = -1;
	}
	else
	{
		m_pActiveDlg = pDialog;
		error = pDialog->DoModal();
		m_pActiveDlg = 0;
	}

	return error;
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_linkEmail.SubclassDlgItem( IDC_EMAIL, this );
	//m_linkWeb.SubclassDlgItem( IDC_URL, this );
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
