//{{NO_DEPENDENCIES}}
// Von Microsoft Visual C++ generierte Includedatei.
// Verwendet durch Teeuhr.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_SETTINGS                    101
#define IDR_MAINFRAME                   128
#define IDI_ICON00                      130
#define IDI_ICON01                      131
#define IDI_ICON02                      132
#define IDI_ICON03                      133
#define IDI_ICON04                      134
#define IDI_ICON05                      135
#define IDI_ICON06                      136
#define IDI_ICON07                      137
#define IDI_ICON08                      138
#define IDI_ICON09                      139
#define IDI_ICON10                      140
#define IDC_WINDOW                      1001
#define IDC_SOUND                       1002
#define IDC_EMAIL                       1003
#define IDC_BING_RADIO                  1010
#define IDC_BING                        1011
#define IDC_BING_SPIN                   1012
#define IDC_FILE_RADIO                  1020
#define IDC_FILE                        1021
#define IDC_FILE_SEARCH                 1022
#define IDC_FILE_PLAY                   1023
#define IDC_STATIC_BGCOLOR              1030
#define IDC_STATIC_FGCOLOR              1031
#define IDC_BGCOLOR                     1032
#define IDC_FGCOLOR                     1033
#define IDC_TEST                        1040
#define IDS_TEEISREADY                  10000
#define IDS_INACTIVE                    10001
#define IDS_RUNNING_MIN_AND_SEC         10002
#define IDS_RUNNING_MIN_OR_SEC          10003
#define IDS_OVERDUE_MIN_AND_SEC         10004
#define IDS_OVERDUE_MIN_OR_SEC          10005
#define IDS_MINUTE                      10006
#define IDS_MINUTES                     10007
#define IDS_SECOND                      10008
#define IDS_SECONDS                     10009
#define IDS_WAV_FILTER                  10010
#define ID_APP_SETTINGS                 32771
#define ID_CANCEL                       32772
#define ID_TIME2                        32773
#define ID_TIME3                        32774
#define ID_TIME4                        32775
#define ID_TIME5                        32776
#define ID_TIME6                        32777
#define ID_TIME8                        32778
#define ID_TIME10                       32779

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1041
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
